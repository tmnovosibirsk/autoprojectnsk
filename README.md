# AutoProjectNsk

## Проект по Java тестированию WEB, API, БД

Автоматизированные Java тесты WEB, API, БД:

1. Парралельный запуск в несколько потоков

2. Параметрезированные тесты

3. Запуск по расписанию в GitLab CI/CD

4. Page object, Page Factory, Page Element, Parallel Execution, Steps, Data-Driven, Pojo, Mock, Cleanup, Builder

5. Результаты тестов разбиты по категориям в Allure:

![Пример изображения](src/test/resources/image.testqastudio/allure2.jpg)

## Используемые проекты

<b>
<details>
  <summary>API</summary>
  <ul>
 <li>https://reqres.in/</li>
  </ul>
</details>

<details>
  <summary>WEB</summary>
  <ul>
 <li>https://testqastudio.me/</li>
  </ul>
</details>

<details>
  <summary>БД</summary>
  <ul>
 <li>Локально MySql</li>
  </ul>
</details>
</b>

## Используемые технологии
```
- Maven
- Gitlab CI/CD
- Java 11
- RestAssured 4.4.0
- Selenium 4.11.0
- Selenide 6.15.0
- Allure 2.13.1
- JUnit 5.8.2
- SLF4J 1.7.32
- Typesafe Config 1.4.1
- WebDriverManager 5.4.1
- JSON Schema Validator 4.3.3
- Lombok 1.18.26
- WireMock (JRE8) 2.32.0
- WireMock Standalone 2.27.2
- Hibernate Core 5.5.6.Final
- MySQL Connector/J 8.0.23
- Javax Persistence API 2.2
- Apache HttpClient 5.2.1
- AssertJ Core 3.20.2
- Faker 1.0.2
- Commons IO 2.11.0
```
## Структура проекта

```
AUTOPROJECTNSK\SRC

├───main
│   ├───java
│   │   └───com
│   │       └───nsk
│   │           ├───api
│   │           │   ├───burg
│   │           │   └───reqres
│   │           │       ├───pojo
│   │           │       │   ├───register
│   │           │       │   ├───resource
│   │           │       │   └───users
│   │           │       └───specifications
│   │           ├───data
│   │           │   └───springboot
│   │           ├───helpers
│   │           └───web
│   │               └───testqastudio
│   │                   ├───elements
│   │                   └───pages
│   └───resources
│       └───META-INF
└───test
    ├───java
    │   ├───api
    │   │   ├───reqres
    │   │   │   ├───json_schema
    │   │   │   ├───negative
    │   │   │   └───positive
    │   │   └───users
    │   ├───database
    │   └───web
    │       ├───testqastudio
    │       │   └───positive
    │       └───users
    └───resources
        └───image.testqastudio

```

## Настройка GitLab CI/CD

```
image: maven:3.8-openjdk-11

default:
  tags:
    - shelllocal

stages:
  - test
  - allure

Integration_to_test:
  stage: test
  cache:
    paths:
      - .m2/repository/
  script:
    - mvn clean test
  only:
    - schedules
    - pushes
    - pipelines
  artifacts:
    paths:
      - target/allure-results/*
  allow_failure: true

Integration_to_allure:
  stage: allure
  script:
    - mvn allure:report
  only:
    - schedules
    - pushes
    - pipelines
  artifacts:
        paths:
        - target/site/allure-maven-plugin/*
        
```

## Test
```
mvn clean test
```

