package database;

import com.nsk.data.springboot.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class TemplateTestPersistenForDoker {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("example-unit"); // "example-unit" - имя persistence unit

        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();

            // Пример JPA-запроса для выборки всех сотрудников с определенным именем
            String jpql = "SELECT e FROM Employee e";
            TypedQuery<Employee> query = em.createQuery(jpql, Employee.class);
//            query.setParameter("employeeName", "John Doe");
            List<Employee> employees = query.getResultList();

            // Вывод результатов
            for (Employee employee : employees) {
                System.out.println(employee);
            }

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emf.close();
        }
    }
}