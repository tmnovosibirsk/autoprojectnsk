package database;

import com.nsk.data.springboot.Employee;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("DATABASE")
@Feature("Hibernate")
public class TestHibernate {

    /**
     * Тест 1
     * 1. Создать класс Employee с параметрами как в таблице  my_db.employee
     * 2. Создать сессию и получить список сотрудников
     * 3. Вывести всех сотрудников с salary < 500 - 3 сотрудника
     * 4. Вывести всех сотрудников с department='HR' - 2 сотрудника
     * 5. Отсортировать сотрудников по зарплате в новый список
     * 6. Создать метод, проверяющий, что сотрудники отсортированы верно.
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Получение списка сотрудников из бд my_db, фильтрация и сортировка")
    public void test() {

        SessionFactory sessionFactory = sessionFactoryFabric(Employee.class);

        try {
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();
            List<Employee> emps = session.createQuery("from Employee").getResultList();

            Long countEmpSalary = emps.stream().filter(x -> x.getSalary() < 500).count();
            assertEquals(countEmpSalary, 3);

            Long countEmpDepartment = emps.stream().filter(x -> x.getDepartment().equals("HR")).count();
            assertEquals(countEmpDepartment, 2);

            List<Employee> sortEmpSalary = emps.stream().sorted(Comparator.comparingInt(Employee::getSalary).reversed()).collect(Collectors.toList());
            assertTrue(listEmployeeIsSort(sortEmpSalary));

        } finally {
            sessionFactory.close();
        }

    }

    public static SessionFactory sessionFactoryFabric(Class clazz) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(clazz)
                .buildSessionFactory();
        return factory;
    }

    public boolean listEmployeeIsSort(List<Employee> list) {
        return IntStream.range(0, list.size() - 1)
                .allMatch(i -> list.get(i).getSalary() >= list.get(i + 1).getSalary());
    }

}

