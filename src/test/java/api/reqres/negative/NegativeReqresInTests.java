package api.reqres.negative;

import com.nsk.api.reqres.pojo.register.UserRegisterRequest;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Epic("API")
@Feature("https://reqres.in/")
public class NegativeReqresInTests {

    private final static String URL = "https://reqres.in/";

    /**
     * Тест 1
     * 1. Используя сервис https://reqres.in/ попробовать зарегистрироваться без password, код ответа = 400,
     * error message = Missing password, создать спецификацию Rest Assured
     */

    @Test
    @Story("Негативные тесты")
    @DisplayName("Регистрация при отсутствии password")
    @Description("При регистрации отсутствует password, код ответа 400, error message = Missing password")
    public void checkEmailUsers() {

        String expectedErrorMessage = "Missing password";

        UserRegisterRequest userRegisterRequest = UserRegisterRequest.builder().email("sydney@fife").build();

        Response response = given()
                .when()
                .contentType(ContentType.JSON)
                .body(userRegisterRequest)
                .post(URL + "api/register")
                .then().log().all()
                .statusCode(400)
                .extract().response();

        String actualErrorMessage = response.jsonPath().get("error");

        assertEquals(expectedErrorMessage, actualErrorMessage);

    }
}
