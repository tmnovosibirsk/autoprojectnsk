package api.reqres.json_schema;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;


@Epic("API")
@Feature("https://reqres.in/")
public class jsonSchemaTest {
    private final static String URL = "https://reqres.in/";

    /**
     * Тест 1
     * 1. Загрузить JSON-схему из файла для валидации
     * 2. Используя сервис https://reqres.in/ получить список пользователей со второй(2) страницы
     * 3. Проверить JSON-ответ по схеме
     */

    @Test
    @Story("Проверкa json схемы")
    @DisplayName("Валидация json schema ответа Users от https://reqres.in/api/users/2")
    public void validateJsonSchema() throws IOException, ProcessingException {
        // Загрузка JSON-схемы из файла
        File schemaFile = new File("src/test/java/api/reqres/json_schema/jsonSchema.json");
        JsonNode schemaNode = JsonLoader.fromFile(schemaFile);

        // Получение JSON-ответа в виде строки
        String responseBody = given()
                .when()
                .get(URL + "api/users/2").getBody().asString();

        JsonNode responseNode = new ObjectMapper().readTree(responseBody);

        // Создание JSON-схемы из загруженной схемы
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonSchema schema = factory.getJsonSchema(schemaNode);

        // Проверка JSON-ответа по схеме
        schema.validate(responseNode);
    }

}
