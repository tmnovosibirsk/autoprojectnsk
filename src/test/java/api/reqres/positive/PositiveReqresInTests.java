package api.reqres.positive;

import com.nsk.api.reqres.pojo.register.UserRegisterRequest;
import com.nsk.api.reqres.pojo.register.UserRegisterResponse;
import com.nsk.api.reqres.pojo.resource.ResourceDataResponse;
import com.nsk.api.reqres.pojo.users.UserDataResponse;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Epic("API")
@Feature("https://reqres.in/")
public class PositiveReqresInTests {
    private final static String URL = "https://reqres.in/";

    /**
     * Тест 1
     * 1. Используя сервис https://reqres.in/ получить список пользователей со второй(2) страницы,
     * статус код = 200, извлечь ответ в POJO класс
     * 2. Убедиться, email начинается с first_name
     * 3. Убедиться, email содержит last_name
     * 4. Убедиться, email заканчивается на reqres.in
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Получение списка пользователей, проверка email")
    @Description("Email каждого пользователя начинается с first_name, содержит last_name, и заканчивается на reqres.in")
    public void checkEmailUsers() {

        List<UserDataResponse> userData = given()
                .when()
                .contentType(ContentType.JSON)
                .get(URL + "api/users?page=2")
                .then().log().all()
                .statusCode(200)
                .extract().body().jsonPath().getList("data", UserDataResponse.class);

        assertTrue(userData.stream().allMatch(x ->
                x.getEmail().startsWith(x.getFirst_name().toLowerCase()) &&
                        x.getEmail().contains(x.getLast_name().toLowerCase()) &&
                        x.getEmail().endsWith("reqres.in")
        ));

    }

    /**
     * Тест 2
     * 1. Используя сервис https://reqres.in/ протестировать регистрацию пользователя в системе, код ответа = 200
     * Написать 2 теста:
     * 1. Используя POJO и паттерн builder
     * 2. Используя класс Response
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Успешная регистрация пользователя в системе")
    public void checkSuccessRegister() {

        String email = "eve.holt@reqres.in";
        String password = "pistol";

        Integer expectID = 4;
        String expectToken = "QpwL5tke4Pnpja7X4";

        //1
        UserRegisterRequest userCreateRequest = UserRegisterRequest
                .builder()
                .email(email)
                .password(password)
                .build();

        UserRegisterResponse userRegisterResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .body(userCreateRequest)
                .post(URL + "api/register")
                .then().log().all()
                .statusCode(200)
                .extract().as(UserRegisterResponse.class);

        Integer actualID = userRegisterResponse.getId();
        String actualToken = userRegisterResponse.getToken();

        assertEquals(actualID, expectID);
        assertEquals(actualToken, expectToken);

        //2
        Map<String, String> register = Map.of("email", email, "password", password);

        Response response = given()
                .when()
                .contentType(ContentType.JSON)
                .body(register)
                .post(URL + "api/register")
                .then().log().all()
                .statusCode(200)
                .extract().response();

        Integer actualID2 = response.jsonPath().get("id");
        String actualToken2 = response.jsonPath().get("token");

        assertEquals(actualID2, expectID);
        assertEquals(actualToken2, expectToken);

    }

    /**
     * Тест 3
     * 1. Используя сервис https://reqres.in/ получить список resource, код ответа = 200
     * 2. Проверить двумя вариантами, что в список отсортирован по годам начиная с 2000
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Получение списка ресурсов, проверка даты")
    @Description("Список полученных ресурсов отсортирован по годам начиная с 2000")
    public void checkSortYearResources() {

        List<ResourceDataResponse> listResourceDataResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .get(URL + "api/unknown")
                .then().log().all()
                .statusCode(200)
                .extract().body().jsonPath().getList("data", ResourceDataResponse.class);

        //1
        List<Integer> actualYears = listResourceDataResponse.stream().map(ResourceDataResponse::getYear).collect(Collectors.toList());
        List<Integer> sortYears = listResourceDataResponse.stream().map(ResourceDataResponse::getYear).sorted().collect(Collectors.toList());

        assertEquals(actualYears, sortYears);

        //2
        int expectYear = 2000;

        assertTrue(IntStream.range(0, listResourceDataResponse.size())
                .allMatch(i -> listResourceDataResponse.get(i).getYear().equals(expectYear + i)));

    }

    /**
     * Тест 4
     * 1. Используя сервис https://reqres.in/ создать и обновить пользователя, коды ответов = 200, 201
     * 2. Убедиться, в ответе при создании и обновлении пользователя, проставляется текущая дата и время
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Создание и обновление пользователя")
    @Description("Пользователь создан, дата создания и обновления = текущей")
    public void createAndUpdateUser() {

        //create
        Map<String, String> createBody = Map.of("name", "morpheus", "job", "leader");

        Response createResponse = given()
                .when()
                .body(createBody)
                .contentType(ContentType.JSON)
                .post(URL + "api/users")
                .then().log().all()
                .statusCode(201)
                .extract().response();

        String actualCreateTime = Instant.now().toString().substring(0, 16);
        String expectCreateTime = createResponse.jsonPath().get("createdAt").toString().substring(0, 16);

        assertEquals(actualCreateTime, expectCreateTime);

        //update
        Map<String, String> updateBody = Map.of("name", "morpheus", "job", "zion resident");

        Response updateResponse = given()
                .when()
                .body(updateBody)
                .contentType(ContentType.JSON)
                .put(URL + "api/users/2")
                .then().log().all()
                .statusCode(200)
                .extract().response();

        String actualUpdateTime = Instant.now().toString().substring(0, 16);
        String expectUpdateTime = updateResponse.jsonPath().get("updatedAt").toString().substring(0, 16);

        assertEquals(actualUpdateTime, expectUpdateTime);

    }

}
