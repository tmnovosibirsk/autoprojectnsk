package web.testqastudio.positive;

import com.nsk.web.BaseSelenideTest;
import com.nsk.web.testqastudio.pages.MainPage;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.File;

@Epic("WEB")
@Feature("https://testqastudio.me")
public class ImagesTests extends BaseSelenideTest {
    private final String URL = "https://testqastudio.me";

    /**
     * Использовать тестовый сайт https://testqastudio.me/, не обращать внимания на специально добавленные ошибки.
     * 1. Перейти на главную страницу тестового сайта https://testqastudio.me
     * 2. Перейти на view 'Горячие товары'
     * 3. Кликнуть на товар 'ЛЕРГРЮН Подвесной светильник'
     * 4. Проверить, что мы перешли на страницу товара с названием 'ЛЕРГРЮН Подвесной светильник'
     * 5. Перейти на картинку товара и скачать её
     * 6. Сравнить заранее подготовленную картинку товара со скаченной
     * 7. Удалить скаченную картинку.
     */

    @ParameterizedTest(name = "{index}:Переход на страницу товара {0} и сравнение изображения с ожидаемым по пути {1}")
    @Story("Позитивные тесты")
    @DisplayName("Проверка соответствия подготовленного изображения ожидаемому на странице товара 'ЛЕРГРЮН Подвесной светильник'")
    @CsvSource({
            "ЛЕРГРЮН Подвесной светильник, src/test/resources/image/testqastudio/legrun.jpg",
            "ДИВВИНА Журнальный столик, src/test/resources/image/testqastudio/divvina.jpg"
    })
    public void compareImageLegrun(String productName, String pathImage) {
        // Путь к подготовленному изображению
        File expectedImage = new File(pathImage);

        new MainPage(URL)
                .goToView("Горячие товары")
                .clickOnProduct(productName)
                .checkProductTitle(productName)
                .downloadAndEqualsProductImage(expectedImage);
    }

}
