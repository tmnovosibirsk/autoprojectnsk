package web.testqastudio.positive;

import com.nsk.helpers.FakerGeneration;
import com.nsk.web.BaseSelenideTest;
import com.nsk.web.testqastudio.pages.MainPage;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Map;


@Epic("WEB")
@Feature("https://testqastudio.me")
public class TestsQaStudio extends BaseSelenideTest {
    private final String URL = "https://testqastudio.me";

    /**
     * Использовать тестовый сайт https://testqastudio.me/, не обращать внимания на специально добавленные ошибки.
     * Тест 1
     * ParameterizedTest, Page object, Faker.
     * Создать параметризированный тест, который принимает значение трёх товаров и суммы этих товаров, использовать Page object, Faker
     * 1. Перейти на главную страницу тестового сайта https://testqastudio.me
     * 2. По очереди добавить товары в корзину 'БРЕДБЕРРИ Комод с ящиками, БРОММС Двухместная кровать, ГРАДВИС Керамическая ваза'
     * 3. Сравнить количество добавленных товаров в модальном диалоге 'Ваша корзина'
     * 4. Сравнить сумму добавленных товаров в модальном диалоге 'Ваша корзина'
     * 5. Кликнуть на 'Оформление заказа'
     * 6. Сравнить общую сумму товаров в корзине
     * 7. С помощью библиотеки Faker, создать метод, который создаёт Map сгенерированных значений. Передать эти значения для заполнения.
     */

    @ParameterizedTest(name = "{index}:Параметризированный тест с добавлением товаров {0}, {1} и проверкой ожидаемой суммы в корзине {2}")
    @Story("Позитивные тесты")
    @DisplayName("Добавления товаров в корзину, сравнение общей суммы и оформления заказа")
    @CsvSource({
            "БРЕДБЕРРИ Комод с ящиками, БРОММС Двухместная кровать, 27100",
            "ГРАДВИС Керамическая ваза, ДИВВИНА Журнальный столик, 9200",
            "ДИННИНГ Табурет со спинкой, КЛЛАРИОН Низкий столик, 10900"
    })
    public void smokeProductSelectionAndOrdering(String product1, String product2, Integer expectedSumm) {

        FakerGeneration fakerGeneration = new FakerGeneration();
        Map<String, String> mapFaker = fakerGeneration.fakerGenerateMap();
        new MainPage(URL)
                .addProductToCart(product1)
                .cartModalDialogIsDisplayed()
                .closeCartModalDialog()
                .addProductToCart(product2)
                .checkCountCartItems(2)
                .checkSummPriceCart(expectedSumm)
                .clickToMakingOrder()
                .checkSummPriceCart(expectedSumm)
                .fillFieldsAndConfirmOrder(
                        mapFaker.get("FirstName"),
                        mapFaker.get("LastName"),
                        mapFaker.get("Company"),
                        mapFaker.get("Address"),
                        mapFaker.get("City"),
                        mapFaker.get("State"),
                        mapFaker.get("PostCode"),
                        mapFaker.get("Phone"),
                        mapFaker.get("Email"),
                        mapFaker.get("OrderComments"));
    }

    /**
     * Тест 2
     * Page object
     * 1. Перейти на главную страницу тестового сайта https://testqastudio.me
     * 2. Проверить, в верхнем меню отображаются и доступны для перехода элементы:'Каталог, Часто задавамые вопросы, Блог, О компании, Контакты'
     * * Реализовать общим классом CommonPageElements - возвращает объект Т, а после класс (который использует данный метод).
     * 3. Перейти на вкладку 'Часто задавамые вопросы', выполнить пункт 2
     * 4. Перейти на вкладку 'Блог', выполнить пункт 2
     * 5. Перейти на вкладку 'О компании', выполнить пункт 2
     * 6. Перейти на вкладку 'Контакты', выполнить пункт 2
     * 7. Перейти на вкладку 'Каталог', выполнить пункт 2
     */

    @Test
    @Story("Позитивные тесты")
    @DisplayName("Верхнее меню, переход на страницы 'Каталог, Часто задавамые вопросы, Блог, О компании, Контакты'")
    public void checkTopMenuAndPages() {

        new MainPage(URL)
                .topMenuDisplayedEnabledEqualsToExpected()
                .goToFaqPage()
                .topMenuDisplayedEnabledEqualsToExpected()
                .goToBlogPage()
                .topMenuDisplayedEnabledEqualsToExpected()
                .goToAboutCompanyPage()
                .topMenuDisplayedEnabledEqualsToExpected()
                .goToContactsPage()
                .topMenuDisplayedEnabledEqualsToExpected()
                .goToMainPage()
                .topMenuDisplayedEnabledEqualsToExpected();
    }

    /**
     * Тест 3
     * Page object
     * 1. Перейти на главную страницу тестового сайта https://testqastudio.me
     * 2. По очереди перейти по view: 'Бестселлеры, Горячие товары, Новые товары'
     * Проверить, на каждой из view, список товаров > 1
     * * Реализовать метод для перехода на view по вводимому тексту
     * 3. Вернуться на страницу 'Все'
     * 4. Добавить в избранное 2 товара: 'ГРАДВИС Керамическая ваза, БРОММС Двухместная кровать'
     * 5. Перейти на страницу 'Список желаний' и убедиться, что значения из шага 4 - добавлены в список
     * 6. Удалить по очереди товары: 'ГРАДВИС Керамическая ваза' БРОММС Двухместная кровать.
     */
    @Test
    @Story("Позитивные тесты")
    @DisplayName("Переход на views и добавление товаров в 'Список желаний'")
    public void checkViewsAndAddItemsToWishListPage() {
        new MainPage(URL)
                .goToView("Бестселлер")
                .goToView("Горячие товары")
                .goToView("Новые товары")
                .goToView("Все")
                .addProductToWishList("ГРАДВИС Керамическая ваза")
                .addProductToWishList("БРОММС Двухместная кровать")
                .goToWishListPage()
                .checkWishList("ГРАДВИС Керамическая ваза")
                .checkWishList("БРОММС Двухместная кровать")
                .removeProductInWishList("ГРАДВИС Керамическая ваза")
                .removeProductInWishList("БРОММС Двухместная кровать")
                .checkWishListIsEmpty();
    }

}
