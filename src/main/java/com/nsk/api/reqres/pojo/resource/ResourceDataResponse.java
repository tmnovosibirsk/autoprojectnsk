package com.nsk.api.reqres.pojo.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class ResourceDataResponse {
   private Integer id;
   private String name;
   private Integer year;
   private String color;
   private String pantone_value;
}
