package com.nsk.web.testqastudio.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.apache.commons.io.FileUtils;


import java.io.File;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.*;

public class ProductPage {

    SelenideElement titleProduct = $x("//h1[@class='product_title entry-title']");
    SelenideElement image = $x("//img[@role='presentation']");

    public ProductPage checkProductTitle(String product) {
        assertEquals(titleProduct.text(), product);
        return this;
    }


    public ProductPage downloadAndEqualsProductImage(File expectedImage) {
        String titleImage = image.getAttribute("src");
        assertNotNull(titleImage);
        try {
            File actualImage = Selenide.download(titleImage);
            FileUtils.contentEquals(expectedImage, actualImage);
            actualImage.delete();
        } catch (Exception e) {
            throw new RuntimeException("Загруженное изображение, не соответствует ожидаемому " + e);
        }
        return this;
    }

}
