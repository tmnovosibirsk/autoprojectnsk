package com.nsk.web.testqastudio.pages;

import com.codeborne.selenide.*;
import com.nsk.web.testqastudio.elements.CartModalDialog;
import com.nsk.web.testqastudio.elements.CommonPageElements;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.codeborne.selenide.Selenide.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainPage extends CommonPageElements<MainPage> {


    private ElementsCollection productsList = $$x("//div[@id='rz-shop-content']//li");

    private SelenideElement buyNow = $x("//div[@class='razzi-slide-button']/a/span");

    private SelenideElement addItem;

    private SelenideElement goToWishList;

    private SelenideElement viewsName;

    private SelenideElement addProductToCart;

    private SelenideElement productToCart;


    public MainPage(String url) {
        Selenide.open(url);
    }

    /**
     * Метод добавляет выбранный по названию товар, в список желаний
     * Ищет необходимый товар, скроллит до него, наводит курсор на элемент 'Добавить в список желаний'
     * Дожидается доступности, кликает на элемент 'Добавить в список желаний',
     * Дожидается изменения иконки 'Добавить в список желаний' на 'Посмотреть список желаний'
     * Иначе товары добавляются слишком быстро, после в 'Списке желаний', часть товаров не появляется
     */
    public MainPage addProductToWishList(String item) {
        String xpathAddItemToWishList = String.format("//a[@data-text='Добавить в список желаний' and @data-product-title='%s']", item);
        addItem = $x(xpathAddItemToWishList);
        addItem.scrollTo();
        addItem.hover();
        addItem.shouldBe(Condition.enabled).click();
        String xpathGoToWishListPage = String.format("//a[@data-title='%s']/..//preceding-sibling::div//a[@data-text='Посмотреть список желаний']", item);
        goToWishList = $x(xpathGoToWishListPage);
        goToWishList.shouldBe(Condition.enabled);
        return this;
    }

    /**
     * Метод перехода на view, с вводимым значением
     * Проверяет, что view доступен и кликает по нему для перехода
     * Прокручивает всю страницу вниз с помощью executeJavaScript
     * Дожидается, список товаров с открытого view > 1
     */
    public MainPage goToView(String view) {
        String xpath = String.format("//div[@class='catalog-toolbar-tabs__content']/a[text()='%s']", view);
        viewsName = $x(xpath);
        viewsName.shouldBe(Condition.enabled).click();
        executeJavaScript("window.scrollTo(0, document.body.scrollHeight);");
        productsList.shouldBe(CollectionCondition.sizeGreaterThan(1));
        return this;
    }


    public CartModalDialog addProductToCart(String item) {
        String xpathProductToCart = String.format("//a[@data-text='В корзину' and @data-title='%s']", item);
        addProductToCart = $x(xpathProductToCart);
        addProductToCart.scrollTo();
        addProductToCart.hover();
        addProductToCart.shouldBe(Condition.enabled).click();
        return new CartModalDialog();
    }



    public ProductPage clickOnProduct(String item) {
        String xpathProductToCart = String.format("//a[@data-title='%s']/../ancestor::div[@class='product-inner']//img", item);
        productToCart = $x(xpathProductToCart);
        productToCart.scrollTo();
        productToCart.hover();
        productToCart.shouldBe(Condition.enabled).click();
        return new ProductPage();
    }





}
