package com.nsk.web.testqastudio.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.nsk.web.testqastudio.elements.CommonPageElements;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FaqPage extends CommonPageElements<FaqPage> {

    private SelenideElement orders = $x("//h4[text()=' Заказы:']");
    private SelenideElement deliveryAndReturn = $x("//h4[text()=' Доставка и возврат:']");
    private SelenideElement payment = $x("//h4[text()=' Оплата:']");


    public FaqPage textOrdersDisplayedAndEquals(String textOrders) {
        orders.shouldBe(Condition.enabled);
        assertEquals(orders.text(),textOrders);
        return this;
    }
    

    public FaqPage textDeliveryAndReturnDisplayedAndEquals(String textDeliveryAndReturn) {
        deliveryAndReturn.shouldBe(Condition.enabled);
        assertEquals(deliveryAndReturn.text(),textDeliveryAndReturn);
        return this;
    }

    public FaqPage textPaymentDisplayedAndEquals(String textPayment) {
        payment.shouldBe(Condition.enabled);
        assertEquals(payment.text(),textPayment);
        return this;
    }


}
