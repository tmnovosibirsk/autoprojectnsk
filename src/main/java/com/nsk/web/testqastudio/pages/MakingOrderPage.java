package com.nsk.web.testqastudio.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.nsk.web.testqastudio.elements.CommonPageElements;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MakingOrderPage extends CommonPageElements<MakingOrderPage> {


    private SelenideElement inputFirstName = $x("//input[@name='billing_first_name']");
    private SelenideElement inputLastName = $x("//input[@name='billing_last_name']");
    private SelenideElement inputCompany = $x("//input[@name='billing_company']");
    private SelenideElement inputAdress1 = $x("//input[@name='billing_address_1']");
    private SelenideElement inputAdress2 = $x("//input[@name='billing_address_2']");
    private SelenideElement inputCity = $x("//input[@name='billing_city']");
    private SelenideElement inputState = $x("//input[@name='billing_state']");
    private SelenideElement inputPostCode = $x("//input[@name='billing_postcode']");
    private SelenideElement inputPhone = $x("//input[@name='billing_phone']");
    private SelenideElement inputEmail = $x("//input[@name='billing_email']");
    private SelenideElement inputOrderComments = $x("//textarea[@id='order_comments']");
    private SelenideElement buttonConfirmOrder = $x("//button[@id='place_order']");
    private SelenideElement actualSumm = $x("//tr[@class='order-total']//bdi");


    public MakingOrderPage checkSummPriceCart(Integer expectedSummItems) {
        Integer actualSummAllProducts = Integer.parseInt(actualSumm.text().replaceAll("[^\\d.]", "").replaceAll("\\.00$", ""));
        assertEquals(actualSummAllProducts, expectedSummItems);
        return this;
    }

    public MakingOrderPage fillFieldsAndConfirmOrder(
            String firstName, String lastName, String company, String address,
            String city, String state, String postCode, String phone, String email, String orderComments) {

        inputFirstName.shouldBe(Condition.enabled);
        inputFirstName.val(firstName);
        inputLastName.val(lastName);
        inputCompany.val(company);
        inputAdress1.val(address);
        inputCity.val(city);
        inputState.val(state);
        inputPostCode.val(postCode);
        inputPhone.val(phone);
        inputEmail.val(email);
        inputOrderComments.val(orderComments);
        buttonConfirmOrder.click();
        return this;

    }


}
