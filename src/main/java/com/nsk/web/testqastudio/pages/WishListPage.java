package com.nsk.web.testqastudio.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.nsk.web.testqastudio.elements.CommonPageElements;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WishListPage extends CommonPageElements<WishListPage> {

    SelenideElement removeProductInWish;

    ElementsCollection productsInWishList = $$x("//form//tbody/tr");


    /**
     * Метод проверяет, есть ли добавленный товар, в списке желаний на страницу 'Список желаний'
     */
    public WishListPage checkWishList(String item) {

        String xpath = String.format("//td[@class='product-name']/a[contains(text(), '%s')]", item);
        SelenideElement listItem = $x(xpath);
        listItem.shouldBe(Condition.enabled);
        return this;

    }

    public WishListPage removeProductInWishList(String product) {
        String xpathRemoveProductInWishList = String.format("//a[@data-title='%s']/ancestor::td/../td[@class='product-remove']//a", product);
        removeProductInWish = $x(xpathRemoveProductInWishList);
        removeProductInWish.shouldBe(Condition.enabled);
        removeProductInWish.click();
        removeProductInWish.shouldBe(Condition.not(Condition.enabled));
        return this;
    }

    public WishListPage checkWishListIsEmpty() {
        productsInWishList.get(0).shouldBe(Condition.text("В списке желаний нет ни одного товара"));
        assertEquals(productsInWishList.size(), 1);
        assertEquals(productsInWishList.get(0).text(), "В списке желаний нет ни одного товара");
        return this;
    }


}
