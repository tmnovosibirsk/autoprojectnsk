package com.nsk.web.testqastudio.elements;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.nsk.web.testqastudio.pages.MainPage;
import com.nsk.web.testqastudio.pages.MakingOrderPage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartModalDialog {


    private SelenideElement cartModalTitle = $x("//div[@id='cart-modal']//div[@class='modal-header']/h3");

    private SelenideElement closeCartModal = $x("//div[@id='cart-modal']//div[@class='modal-header']/a");

    private ElementsCollection hrefTextProducts = $$x("//div[@id='cart-modal']//ul/li//div[@class='woocommerce-mini-cart-item__summary']/span/a");

    private ElementsCollection priceEachProducts = $$x("//div[@class='cart-panel-content panel-content']//li//span[@class='woocommerce-Price-amount amount']");

    private SelenideElement priceAmount = $x("//div[@class='widget_shopping_cart_footer']//span[@class='woocommerce-Price-amount amount']");

    private SelenideElement hrefMakingOrder = $x("//div[@class='widget_shopping_cart_footer']//a[text()='Оформение заказа']");

    private SelenideElement hrefViewShoppingCart = $x("//div[@class='widget_shopping_cart_footer']//a[text()='Посмотреть корзину']");


    public CartModalDialog cartModalDialogIsDisplayed() {
        cartModalTitle.shouldBe(Condition.visible).shouldBe(Condition.enabled);
        return this;
    }


    /**
     * Проверка количества добавленных товаров в корзину
     * Перед проверкой, выполняем метод MainPage.addItemToCart
     */
    public CartModalDialog checkCountCartItems(Integer countExpectedItems) {
        cartModalTitle.shouldBe(Condition.enabled);
        String expectedLoadText = String.valueOf(countExpectedItems);
        cartModalTitle.shouldHave(text(expectedLoadText));
        Integer actualCountItems = Integer.parseInt(cartModalTitle.text().replaceAll("\\D", ""));
        //Пример: Ваша Корзина (3), проверяем количество товаров по тексту из заголовка
        assertEquals(countExpectedItems, actualCountItems);
        //Проверяем список фактически добавленных товаров
        assertEquals(countExpectedItems, hrefTextProducts.size());
        return this;
    }


    public MainPage closeCartModalDialog() {
        closeCartModal.shouldBe(Condition.enabled);
        closeCartModal.click();
        return new MainPage();
    }

    /**
     * Считаем сумму всех товаров в корзине.
     * Для этого убираем из элементов лишние символы
     * Пример:String '1,200.00 ₽', после преобразования int 1200
     * Далее сравниваем со значениям в поле 'Подытог:'
     */
    public CartModalDialog checkSummPriceCart(Integer expectedSumm) {
        priceEachProducts.shouldBe(CollectionCondition.sizeGreaterThan(0));
        Integer actualSummAllProducts = priceEachProducts
                .texts()
                .stream()
                .map(x -> Integer.parseInt(x.replaceAll("[^\\d.]", "")
                        .replaceAll("\\.00$", "")))
                .mapToInt(x -> x).sum();
        Integer expectedSummAllProducts = Integer.parseInt(priceAmount.text().replaceAll("[^\\d.]", "").replaceAll("\\.00$", ""));
        assertEquals(actualSummAllProducts, expectedSumm);
        assertEquals(actualSummAllProducts, expectedSummAllProducts);
        return this;
    }


    public MakingOrderPage clickToMakingOrder() {
        hrefMakingOrder.shouldBe(Condition.enabled).click();
        return new MakingOrderPage();
    }


}
