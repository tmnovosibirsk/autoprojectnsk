package com.nsk.web.testqastudio.elements;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.nsk.web.testqastudio.pages.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonPageElements<T extends CommonPageElements> {

    private SelenideElement headerCatalog = $x("//ul[@id='menu-top']//a[text()='Каталог']");
    private SelenideElement headerFaq = $x("//ul[@id='menu-top']//a[text()='Часто задавамые вопросы']");
    private SelenideElement headerBlog = $x("//ul[@id='menu-top']//a[text()='Блог']");
    private SelenideElement headerAboutCompany = $x("//ul[@id='menu-top']//a[text()='О компании']");
    private SelenideElement headerContacts = $x("//ul[@id='menu-top']//a[text()='Контакты']");

    private SelenideElement wishList = $x("//a[@class='wishlist-icon']");

    private ElementsCollection headerMenuItems = $$x("//ul[@id='menu-top']/li/a");


    public WishListPage goToWishListPage() {
        wishList.shouldBe(Condition.enabled).click();
        return new WishListPage();

    }

    public MainPage goToMainPage() {
        headerCatalog.shouldBe(Condition.enabled).click();
        return new MainPage();
    }

    public FaqPage goToFaqPage() {
        headerFaq.shouldBe(Condition.enabled).click();
        return new FaqPage();
    }

    public BlogPage goToBlogPage() {
        headerBlog.shouldBe(Condition.enabled).click();
        return new BlogPage();
    }

    public AboutCompanyPage goToAboutCompanyPage() {
        headerAboutCompany.shouldBe(Condition.enabled).click();
        return new AboutCompanyPage();
    }

    public ContactsPage goToContactsPage() {
        headerContacts.shouldBe(Condition.enabled).click();
        return new ContactsPage();
    }


    /**
     * Проверка шапки страницы,список соответствует 5 значениям:
     * Каталог, Часто задавамые вопросы, Блог, О компании, Контакты
     * Каждый элемент isEnabled
     * Метод можно переиспользовать в классах наследниках, тип данных, будет возвращаться
     * в зависимости от используемого класса
     */
    public T topMenuDisplayedEnabledEqualsToExpected() {

        List<String> expectedItems = new ArrayList<>();
        expectedItems.add("Каталог");
        expectedItems.add("Часто задавамые вопросы");
        expectedItems.add("Блог");
        expectedItems.add("О компании");
        expectedItems.add( "Контакты");

        headerMenuItems.shouldHave(texts(expectedItems));

        headerMenuItems.forEach(x -> x.shouldBe(Condition.enabled));

        return (T) this;

    }




}
