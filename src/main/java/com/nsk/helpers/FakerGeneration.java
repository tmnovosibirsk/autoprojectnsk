package com.nsk.helpers;

import com.github.javafaker.Faker;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class FakerGeneration {

    private final Faker faker = new Faker();


    public Map<String, String> fakerGenerateMap() {
        Map<String, String> fakerMap = new HashMap<>();
        fakerMap.put("FirstName", faker.name().firstName());
        fakerMap.put("LastName", faker.name().lastName());
        fakerMap.put("Company", faker.company().name());
        fakerMap.put("Address", faker.address().streetAddress());
        fakerMap.put("City", faker.address().city());
        fakerMap.put("State", faker.address().state());
        fakerMap.put("PostCode", faker.address().zipCode());
        fakerMap.put("Phone", faker.phoneNumber().phoneNumber());
        fakerMap.put("Email", faker.internet().emailAddress());
        fakerMap.put("OrderComments", faker.lorem().sentence());
        return fakerMap;

    }

}
